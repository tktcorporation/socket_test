Rails.application.routes.draw do
  post '/room', to: 'room#room'
  get '/room/', to: 'room#room'
  get '/room/:id', to: 'room#room'
  get '/home/top'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
