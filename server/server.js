var fs = require("fs");
var server = require("http").createServer(function(req, res) {
    res.writeHead(200, { "Content-Type": "text/html" });
    var output = fs.readFileSync("./index.html", "utf-8");
    res.end(output);
}).listen(3030);
var io = require("socket.io").listen(server);

var userHash = {};

io.sockets.on("connection", function(socket) {
    var room = '';
    var name = '';

    // roomへの入室は、「socket.join(room名)」
    socket.on('client_to_server_join', function(join_data) {
        var id = socket.id;
        room = join_data.roomId;
        name = join_data.name;
        socket.join(room);
        var personalMessage = "あなたは、" + name + "さんとしてルーム" + room + "に入室しました。";
        io.sockets.to(id).emit('publish', { value: personalMessage });
        var broadcastMessage = name + "さんが入室しました。";
        socket.broadcast.to(room).emit('publish', { value: broadcastMessage });
    });

    socket.on("connected", function(name) {
        var msg = name + "が入室しました";
        userHash[socket.id] = name;
        io.sockets.emit("publish", { value: msg });
    });

    socket.on("client_to_server_all", function(data) {
        io.sockets.emit("publish", { value: data.value });
    });

    socket.on("client_to_server_room", function(data) {
        io.sockets.to(room).emit('publish', { value: data.value });
    });

    socket.on("client_to_server_disconnect", function() {
        socket.disconnect();
    });

    socket.on("client_to_server_leave_room", function() {
        socket.leave(room);
    });

    socket.on("leave_all_rooms", function() {
        var rooms = io.sockets.adapter.sids[socket.id];
        console.log("server.js:" + "method:" + "leave_all_rooms:" + "comment:" + "roomsが取得できているか:" + rooms)
        for (var room in rooms) {
            socket.leave(room);
        }
    });

    socket.on("disconnect", function() {
        if (name) {
            var msg = name + "が退出しました";
            name = "";
            io.sockets.to(room).emit("publish", { value: msg });
        }
        if (userHash[socket.id]) {
            var msg = name + "が退出しました";
            userHash[socket.id] = "";
            io.sockets.emit("publish", { value: msg });
        }
    });
});